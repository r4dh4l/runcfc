# ruNCFC – **r**4dh4l’s **u**ltimate **N**ecromunda **C**ampaign **F**ighter **C**ards

A [tabletop](https://en.wikipedia.org/wiki/Miniature_wargaming) project for a better [Necromunda](https://en.wikipedia.org/wiki/Necromunda) Gang Fighter Card.

[![runcfc_preview](media/runcfc_preview.png "ruNCFC preview picture")](https://gitlab.com/r4dh4l/runcfc/-/blob/main/runcfc_4on1.pdf)

# General idea

The idea of this Necromunda Gang Fighter Card is to have better track possibilities of a Ganger's progress during a campaign, for example to track:
- permanent Injuries,
- experience points and how you spent them and
- a log for each match to track on the Fighter Card:
    - Kills the Ganger made,
    - Flesh Wounds the Ganger got,
    - status for "Serious Injury" ("**SI**"), "Out of Action" ("skull and crossbones" symbol) and Recovery ("**R**").

You may notice some additional fields in comparison to the original Gang Roster: `Fighter ID` and `Arena Fighter Glory` are fields for an internal campaign for which the author created this custom Fighter Cards.
- The `Fighter ID` is a way to identify Gangers by a unique, short ID next to the fighter name ([ruNCFC](https://gitlab.com/r4dh4l/runcfc/) describes a way how to generate it). The idea is to have a very effective way to identify Gang fighters. For example: If you are in a rush and have to recruit a new Gang fighter and have not a good idea for a Fighter name right now you already have an ID for the Fighter so you can name the Fighter later. Or if you capture an opponent Gang fighter you can just ask for the Fighter ID instead of writing down crazy, long Fighter names.
- The `Arena Fighter Glory` is a way to track the success of the Gang Fighter's Fighting Pit matches. You maybe don't need this field and can just ignore or modify it for your demands. 

These Fighter Cards are intended to be used in combination with [ruNCGR](https://gitlab.com/r4dh4l/runcgr/) (**r**4dh4l’s **u**ltimate **N**ecromunda **C**ampaign **G**ang **R**oster).

# Purpose of the files

## [runcfc_4on1.pdf](https://gitlab.com/r4dh4l/runcfc/-/blob/main/runcfc_4on1.pdf)

**This is the file you surely want to use in a Necromunda campaign printing it several times and filling it out by hand (I suggest using a pencil).** It is a PDF export from `runcfc.odt` as 4 pages on 1 in landscape mode. Unfortunately it is not digitally fillable yet.

## [runcfc.pdf](https://gitlab.com/r4dh4l/runcfc/-/blob/main/runcfc.pdf)

This is a PDF export from `runcfc.odt`for the case the `runcfc_4on1.pdf` is too small for your eyes to read. It is not digitally fillable yet as well.

## [runcfc.odt](https://gitlab.com/r4dh4l/runcfc/-/blob/main/runcfc.odt)

`runcfc.odt` can be used
- to adjust the file for your demands before printing it or
- to export the file to PDF files different to the one provided here.

It is an **O**pen**D**ocument**T**ext formated file created with [LibreOffice](https://www.libreoffice.org/) (you can use any other office program which can handle ODT to edit the file but please share derivates of the file as ODT as well so that a freedom respecting handling of the file is ongoing possible). 

To export a modified `runcfc.odt` as 4on1 PDF (like `runcfc_4on1.pdf`) you have to print the ODT file as a file using the following settings under `File` -> `Print` -> `General` tab:
- Section "Printer": `Print to Files...`
- Section "Range and Copies" -> "Pages": `1,1,1,1`
- Section "Page Layout" -> "more" -> "Pages per sheet": `4`

Please keep in mind to respect the [license](https://gitlab.com/r4dh4l/runcfc/-/blob/main/LICENSE.md) making your modifications. You are very welcome to use (even commercially), share and improve this document as long as you grant others the same to your adaptions (means: you are not allow to change the license CC BY-SA).
